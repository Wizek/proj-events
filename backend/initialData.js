function nowPlusNDays(days) {
  var date = new Date(Date.now() + days * 86400000)
  return date.toISOString()
}

const rTemp = () => Math.round(-10 + Math.random() * 40)

module.exports = [
  {
    title: "The Historic Hotels of New York's Upper East Side' Webinar",
    description: `While New York City's hotels of the modern era are geared for domestic and international visitors, that wasn't always the case. Hotels built in the 19th and 20th centuries were originally erected as permanent residences for the richest New Yorkers, who were looking to downsize from their palatial mansions on Fifth Avenue into 'apartment hotels' filled with top-of-the-line accommodations. It's time to explore the luxurious and sumptuous hotels east of Central Park that welcomed the highest echelon of visitors, along with a fair share of drama and debauchery.`,
    start: nowPlusNDays(-1),
    location: {name: "Budapest", country:"HU"},
    weather: {temp:{min:rTemp(), max:rTemp()}, icon:'01d'},
  },
  {
    title: "Sounds From Scotland",
    description: `Please join us for our new revamped monthly Sounds From Scotland program live on the American Scottish Foundation Facebook page on Sunday April 18th from 4PM - 5PM EDT for our monthly music program featuring interviews, music videos, news and updates on music festivals in the US, Scotland and more!
On Sunday April 18th we'll be joined by Stuart Cassells - founder of the internationally renowned Scottish music superstars Red Hot Chilli Pipers. Stuart will discuss with us the origins of Red Hot Chilli Pipers and how they took the world by storm.`,
    start: nowPlusNDays(0.05),
    location: {name: "New York", country:"US"},
    weather: {temp:{min:rTemp(), max:rTemp()}, icon:'01d'},
  },
  {
    title: "Mad Hatter's (Gin &) Tea Party – New York",
    description: `- An immersive, 1.5 hour-long cocktail experience with teatime snacks hosted by the Hatter himself. 🎩
- Get started with a small welcome drink and then 3 crazy craft cocktails in this mad tea party all while wearing a Mad Hatter hat. 🍹🍵
- All taking place in a secret Wonderland garden somewhere in New York City 🐇🍄`,
    start: nowPlusNDays(1),
    location: {name: "Bratislava", country:"SK"},
    weather: {temp:{min:rTemp(), max:rTemp()}, icon:'01d'},
  },
  {
    title: "Live Bachata at Sahadi's Industry City!",
    description: `Come see Grupo Aurora perform live Bachata at Sahadi's Industry City in Courtyard 3/4 on Sunday, April 18th 3-6PM! Please call 718-788-7500 for reservations.
Grupo Aurora is an energizing band from New York City whose goal is to revitalize the Bachata music scene through creativity, soul and showmanship. Their sound fuses the soul and bitterness of traditional Bachata with modern production: A concept they call ‘Amargue Moderno’`,
    start: nowPlusNDays(2),
    location: {name: "Tokyo", country:"JP"},
    weather: {temp:{min:rTemp(), max:rTemp()}, icon:'01d'},
  },
  {
    title: "Miwa Gemini plays OperationGig!",
    description: `Miwa Gemini - all female trio led by Japanese singer/songwriter Miwa Nishio will have you laughing, dancing and crying all at the same time. They sing anything from grey hair to summer fruits, fairy tales to motherhood woes, deliver each song with classic water tight harmony and unique instrumentation.`,
    start: nowPlusNDays(15),
    location: {name: "Bejing", country:"CN"},
    weather: {temp:{min:rTemp(), max:rTemp()}, icon:'01d'},
  },
]
