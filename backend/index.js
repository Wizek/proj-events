const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const port = 3000
const app = express()
const startingData = require('./initialData.js')
const dummyDatabase = Object.fromEntries(startingData.map((o, id) => [id + 1, { id: id + 1, ...o }]))
let highestIndex = startingData.length + 1

app.use(cors())
app.use(bodyParser.json())

app.get('/events', (req, res) => {
  res.send(Object.values(dummyDatabase))
})
app.get('/events/:id', (req, res) => {
  const lookup = dummyDatabase[req.params.id]
  res.send(lookup || 404)
})
app.post('/events', (req, res) => {
  const id = highestIndex++
  const out = dummyDatabase[id] = { id, ...req.body }
  res.send(out)
})
app.delete('/events/:id', (req, res) => {
  delete dummyDatabase[req.params.id]
  res.send(200)
})

app.listen(port, () => console.log(`Listening on port ${port}`))
