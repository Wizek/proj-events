import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueTimeago from 'vue-timeago'

import vuetify from './plugins/vuetify'
import Toasted from 'vue-toasted'
import Loading from '@/components/Loading.vue'

Vue.component('Loading', Loading)

Vue.use(Toasted, {
  position: 'bottom-left',
  duration: 10_000,
  keepOnHover: true,
  action: [
    {
      text: 'Dismiss',
      onClick: (e, toastObject) => {
        toastObject.goAway(0);
      }
    },
  ]
})

Vue.use(VueTimeago, { locale: 'en' })

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
