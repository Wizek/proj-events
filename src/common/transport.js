export const postReqJson = async (url, payload) => {
  const res = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {
      'Content-Type': 'application/json',
    },
  })
  handleError(res)
  const obj = await res.json()
  return obj
}

export const getReqJson = async (url) => {
  const res = await fetch(url)
  handleError(res)
  const obj = await res.json()
  return obj
}

export const deleteReq = async (url) => {
  const res = await fetch(url, {
    method: 'DELETE',
  })
  handleError(res)
  return res
}

const handleError = res => {
  if (!res.ok) {
    res.errorLocation = new Error(res.statusText)
    res.toString = () => res.statusText
    throw res
  }
}
