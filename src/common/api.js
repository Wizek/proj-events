import {postReqJson, getReqJson, deleteReq} from '@/common/transport.js'

export const readEvents  = () =>  getReqJson(`${apiBase}/events`)
export const readEvent   = id =>  getReqJson(`${apiBase}/events/${id}`)
export const deleteEvent = id =>   deleteReq(`${apiBase}/events/${id}`)
export const createEvent = pl => postReqJson(`${apiBase}/events/`, pl)

export const geoApi      = q  => getReqJson(geoApiUrl(q))
export const weatherApi  = o  => getReqJson(weatherApiUrl(o))

export const apiBase =
  `${location.protocol}//${location.hostname}:3000`

const openweathermapKey = '30f42667547f785e0c231b503c1cabc7'
export const geoApiUrl = q =>
  `${location.protocol}//api.openweathermap.org/geo/1.0/direct?q=${encodeURIComponent(q)}&limit=10&appid=${openweathermapKey}`

export const weatherApiUrl = ({lat, lon}) =>
  `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=none&units=metric&appid=${openweathermapKey}`
