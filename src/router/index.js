import Vue from 'vue'
import VueRouter from 'vue-router'
import UpcomingOrPastView from '../views/UpcomingOrPastView'
import CreateView from '../views/CreateView'
import EventDetailsView from '../views/EventDetailsView'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'UpcomingView',
    component: UpcomingOrPastView,
  },
  {
    path: '/past',
    name: 'PastView',
    component: UpcomingOrPastView,
  },
  {
    path: '/event/:id',
    name: 'EventDetailsView',
    component: EventDetailsView,
  },
  {
    path: '/edit/:id',
    name: 'EventEditView',
    component: EventDetailsView,
  },
  {
    path: '/create',
    name: 'Create',
    component: CreateView,
  }
]

const router = new VueRouter({
  routes
})

export default router
